-----------------------------------------------------------------------------------------
--
-- utils.bulkFileDownloader.lua
--
-- Utility class for downloading several files at once
--
-----------------------------------------------------------------------------------------

BulkFileDownloader = {}

function BulkFileDownloader:new()
    local downloader = {}
	
	downloader.fileCounter = 0;
	downloader.filenamePrefix = "tmp_" .. math.random(100000, 999999) .. "_"
	
	function downloader:getFilename(url)
	    self.fileCounter = self.fileCounter + 1
		return self.filenamePrefix .. self.fileCounter
	end
	
	--Provides invocation of "download finished" callback for each specified URL in order corresponding to its position in the input list
	function downloader:downloadKeepingOrder(urlList, storageDir, onFileDownloadFinishedListener)
	    local filenames = {}
		for i = 1, #urlList do
	        filenames[i] = self:getFilename(urlList[i])
        end
		
		--to be filled in the order corresponding to enumeration of URL's in urlList
		local downloadFinishedEvents = {}
		
		local counterOfFinished = 0
		
		local downloadListener = function(event)
		    local index = table.indexOf(filenames, event.response.filename)
			if ( event.isError ) then
        		print( "Network error - download failed: ", event.response )								
				counterOfFinished = counterOfFinished + 1
				if (index ~= nil) then
					downloadFinishedEvents[index] = event
				end
    		elseif ( event.phase == "ended" ) then
                downloadFinishedEvents[index] = event
        		counterOfFinished = counterOfFinished + 1
    		end
			
			if (counterOfFinished == #filenames) then
			    for i = 1, #downloadFinishedEvents do
				    local event = downloadFinishedEvents[i]
					if (event ~= nil) then
						onFileDownloadFinishedListener(event)				
					end	
                end
			end
		end

		for i = 1, #urlList do
	        network.download( urlList[i], "GET", downloadListener, filenames[i], storageDir)
        end
	end
	
	return downloader
end

return BulkFileDownloader