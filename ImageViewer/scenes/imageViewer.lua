-----------------------------------------------------------------------------------------
--
-- scenes.imageViewer.lua
--
-----------------------------------------------------------------------------------------

require( "utils.bulkFileDownloader" )

local json = require( "json" )
local widget = require( "widget" )
local composer = require( "composer" )

local scene = composer.newScene()

local scrollView = widget.newScrollView(
    {
        top = 0,
        left = 0,
        width = display.contentWidth,
        height = display.contentHeight,
        horizontalScrollDisabled = true		
    }
) 

local function reduceSizeToFitMaxWidth(img, maxWidth)
    if (img.width > maxWidth) then
		local ratio = maxWidth / img.width
		img.width = maxWidth
		img.height = img.height * ratio
	end
end

local separationGap = 10;
local totalContentHeight = 0;

local function placeImage(img)
    reduceSizeToFitMaxWidth(img, scrollView.width)
    scrollView:insert(img)
	img.x = scrollView.width / 2
	if (totalContentHeight > 0) then
	    totalContentHeight = totalContentHeight + separationGap
	end
	img.y = totalContentHeight + img.height / 2
	totalContentHeight = totalContentHeight + img.height
end

local function onImageLoaded(event)
	if ( event.isError ) then
        print( "Network error - download failed: ", event.response )
    elseif ( event.phase == "ended" ) then        
        local img = display.newImage(event.response.filename, event.response.baseDirectory, display.contentCenterX, display.contentCenterY)		
		placeImage(img)
    end
end

local function getImageUrls()
    local file = io.open( system.pathForFile( "assets/images.json", system.ResourceDirectory ), "r" )
    local contents = file:read( "*a" )
    io.close( file )
    return json.decode( contents ).images
end

local function showImages()
	local downloader = BulkFileDownloader:new()
	downloader:downloadKeepingOrder(getImageUrls(), system.TemporaryDirectory, onImageLoaded)
end

function scene:create( event )
	local sceneGroup = self.view	
	sceneGroup:insert(scrollView)
	showImages()	
end

function scene:show( event )	
end

function scene:hide( event )
end

function scene:destroy( event )
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene